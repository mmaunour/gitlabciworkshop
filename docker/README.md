This directory contains a `Dockerfile` to generate a Docker image that contains
all necessary dependencies that the `projection` package requires. This image
can be pushed to the GitLab registry to be used for CI jobs.

## Build Docker image

To create the Docker image with name `projection` and tag `v1`, execute the
following command from project root directory:

```bash
  docker image build -t projection:v1 -f docker/Dockerfile .
```
